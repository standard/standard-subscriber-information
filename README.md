# Standard subscriber information

Thank you for your purchasing a standard GitLab subscription!

We strongly advise our standard subscribers to plan an intake immediately after subscribing. In the intake, our engineers will go over some of our best practices, so as to make sure that your GitLab installation is robust and secure. Please contact our engineers at subscribers@gitlab.com to book an intake session.

If you would like to receive access to GitLab Enterprise Edition and the Standard subscribers documentation please create an account on https://gitlab.com/users/sign_up and send us the username. Note that your user will be visible to other GitLab Basic and Standard Subscribers.

You can find the Enterprise Edition repository here:
https://gitlab.com/subscribers/gitlab-ee

You can find documentation for a highly available setup here:
https://gitlab.com/groups/standard

Packages:
https://gitlab.com/subscribers/gitlab-ee/blob/master/doc/install/packages.md

Documentation:
http://doc.gitlab.com/ee/

To upgrade from CE, just perform a normal upgrade, but make use of an EE package:
https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/update.md#updating-from-gitlab-6-6-x-and-higher-to-the-latest-version

This standard subscription entitles you to 24/7 support from our side.
For critical availability incidents, you can contact us at emergency@gitlab.com.
For all technical questions or if you want to schedule a regular call, please contact us at subscribers@gitlab.com.
For all other questions, contact us at sales@gitlab.com 
